package main

import "github.com/codegangsta/martini"
import "github.com/codegangsta/martini-contrib/render"
import "os/exec"
import "net/http"
import "bytes"
import "log"
import "fmt"
import "regexp"

// TODO break this into functions/methods
// Puppetize deployment
// write a 404 template, etc

const (
    Homecode string = "A1"
)

type X10Req struct {
    Housecode string
    Requri    string
    Host      string
}

func main() {
    m := martini.Classic()
    http.Handle("/", m)
    m.Use(render.Renderer())

    m.Get("/", func(r render.Render, params martini.Params, req *http.Request) {
        event := X10Req{Homecode, req.RequestURI, req.Host}

        mobile, _ := regexp.MatchString("(?i)Ipod|(?)Ipad|(?)Android", req.Header["User-Agent"][0])
        if mobile == true {
            r.HTML(200, "mobile", event)
        } else {
            r.HTML(200, "desktop", event)
        }
    })

    m.Get("/"+Homecode+"/:action", func(r render.Render, params martini.Params, req *http.Request) {
        //TODO if action not on or off, bail
        event := X10Req{Homecode, req.RequestURI, req.Host}
        if params["action"] != "on" && params["action"] != "off" {
            log.Fatal("Error, action must be on or off.")
        }

        cmd := exec.Command("sudo", "heyu", "f"+params["action"], Homecode)
        command := "sudo heyu f" + params["action"] + " " + Homecode
        println("Command: " + command)
        var out bytes.Buffer
        cmd.Stdout = &out
        err := cmd.Run()
        if err != nil {
            log.Fatal(err)
        }
        fmt.Printf(" %q\n", out.String())
        r.HTML(200, params["action"], event)
    })

    m.Run()
}
